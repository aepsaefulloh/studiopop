@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title', 'Home')
@section('content')
    @include('components.include.header')

    <!-- Breadcrumbs -->
    <section class="section-timezone text-center">
        <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>
    </section>

    <!-- Hero -->
    <section class="section-hero">
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                @foreach ($hero_banner as $item)
                    <div class="swiper-slide">
                        <a @if ($item['CATEGORY'] == 1)href="{{ url('/journal-detail') . '/' . $item['ID'] . '/' . Helper::url_slug($item['TITLE']) }}" @elseif($item['CATEGORY']==2)href="{{ url('/pop-n-roll-detail') . '/' . $item['ID'] . '/' . Helper::url_slug($item['TITLE']) }}" @endif>
                            <div class="hero image-as-background" style="background-image: url('{{ $item['IMAGE'] }}');">
                                <header class="hero-container">
                                    <h1 class="hero-title">{{ $item['SUBTITLE'] }}</h1>
                                    <p class="">{{ $item['TITLE'] }}</p>

                                </header>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>
    <!-- Hero-End -->

     <!-- section Article -->
     <section class="section-article my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="main-heading">
                        {{-- <p>VIEW THE COLLECTION</p> --}}
                        <h1>Project</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="grid-products">
                        @forelse ($project as $item)
                            <figure class="figure">
                                <a
                                    href="{{ url('/project-detail') . '/' . $item['ID'] . '/' . Helper::url_slug($item['TITLE']) }}">
                                    <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid " alt="product">
                                    <figcaption class="figure-caption">{{ $item['TITLE'] }}</figcaption>
                                </a>
                            </figure>
                        @empty
                            <h5 class="text-center">Data Masih Kosong!</h5>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="row text-center mt-5">
                <div class="col-12">
                    <button class="btn btn-dark" onclick="window.location.href='{{ url('/project') }}';">Load
                        More</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Article End -->

    <!-- Section Product -->
    <section class="section-product my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="main-heading">
                        {{-- <p>VIEW THE COLLECTION</p> --}}
                        <h1>Store</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                @forelse ($product as $item)
                    <div class="col-6 col-md-3">
                        <div class="product-box">
                            <figure class="figure">
                                <a href="{{ url('/store') }}">
                                    <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid rounded"
                                        alt="{{ $item['IMAGE'] }}">
                                    <figcaption class="figure-caption">
                                        <a href="{{ url('/store') }}">See More&nbsp;&nbsp;<i
                                                class="bi bi-arrow-right"></i></a>
                                    </figcaption>
                                </a>
                            </figure>
                        </div>
                    </div>
                @empty
                    <h5 class="text-center">Data Masih Kosong!</h5>
                @endforelse
            </div>
        </div>


    </section>
    <!-- Section Product End -->

   

    @include('components.include.footer')

@endsection

@push('scripts')
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 1,
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },

        });
    </script>
@endpush
