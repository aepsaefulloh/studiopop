@extends('components.master')
@section('csrf_include')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


@section('title', 'Cart')
@section('content')
@include('components.include.header')


<!-- Breadcrumbs -->
<section class="section-timezone ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center align-self-center">
                <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>
            </div>
        </div>
    </div>
</section>

<section class="section-cart my-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <div class="d-flex justify-content-between">
                        <h1>Cart</h1>
                        <a class="btn btn-danger" href="{{ route('remove_cart') }}">Empty Cart</a>
                    </div>
                    <div class="line1"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" class="text-center">Price</th>
                        <th scope="col" class="text-center">Quantity</th>
                        <th scope="col" class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ((isset($_SESSION['CART']) && isset($_SESSION['CART']['DATA'])) ? $_SESSION['CART']['DATA'] : [] as $item)
                        <tr>
                            <td class="td-img">
                                <div class="checkout-item">
                                    <img src="{{ $item['IMAGE'] }}" class="item-image" alt="{{ $item['IMAGE'] }}">
                                    <div class="checkout-info">
                                        <p class="checkout-name">{{ $item['NAME'] }}</p>
                                        <p>
                                            @if ($item['SIZE'] == 1)
                                                S
                                            @elseif ($item['SIZE'] == 2)
                                                M
                                            @elseif ($item['SIZE'] == 3)
                                                L
                                            @elseif ($item['SIZE'] == 4)
                                                XL
                                            @endif
                                        </p>
                                        <p>{{ number_format($item['PRICE'], 0, ',', '.').'x'.$item['QTY'] }}</p>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">Rp. {{ number_format($item['PRICE'], 0, ',', '.') }}</td>
                            <td class="text-center">{{ $item['QTY'] }}</td>
                            <td class="text-center">Rp. {{ number_format($item['COUNT_PRICE'], 0, ',', '.') }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center">Data Masih Kosong!</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="2">
                        </td>
                        <td class="text-center">
                            TOTAL ITEM :
                        </td>
                        <td class="text-center">
                            Rp. @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ number_format($_SESSION['CART']['COUNT_PRICE'], 0, ',', '.') }} @else 0 @endif
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="row text-center mt-5">
            <div class="col-12">
                <div class="group-button" role="group" aria-label="Basic example">
                    {{-- <div onclick="window.location.href='{{url('/profile')}}';" class="btn btn-outline-secondary rounded-0 px-5">Continue</div> --}}
                    <div onclick="window.location.href='{{url('/profile')}}';" class="btn btn-outline-secondary rounded-0 px-5 @if (!$_SESSION || (isset($_SESSION['CART']['DATA']) && count($_SESSION['CART']['DATA']) == 0)) disabled @endif">Checkout</div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('components.include.footer')
@endsection
