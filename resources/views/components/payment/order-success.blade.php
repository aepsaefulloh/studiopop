@extends('components.master')
@section('title', 'Home')
@section('content')
@include('components.include.header')


<!-- Breadcrumbs -->
<section class="section-timezone ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center align-self-center">
                <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>
            </div>
        </div>
    </div>
</section>


<section class="section-order-success my-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="main-heading text-md-center">
                    <h3>Store</h3>
                    <p>Thank You | {{ \Carbon\Carbon::now()->format('d-m-Y') }}</p>
                    <div class="line1"></div>
                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12">
                <div class="text-order text-center">
                    <p>Hi, {{ $_SESSION['CHECKOUT_SUCCESS']['NAME']  }}</p>
                    <p>Thank you for your order! Your items will be delivered after the payment has been made.<br>
                        Please make your payment within 24 hours to avoid order cancellation and send the payment
                        receipt to shopping@studiopop.id</p>
                    <div class="item my-5">
                        <p>No Order : {{ $_SESSION['CHECKOUT_SUCCESS']['NO_TR']  }}</p>
                        <p>Shipping : </p>
                        <ul>
                            @foreach ($_SESSION['CHECKOUT_SUCCESS']['PRODUCT'] as $item)
                                <li><p>- {{ $item }}</p></li>
                            @endforeach
                        </ul>
                        <p>Total : Rp. {{ number_format($_SESSION['CHECKOUT_SUCCESS']['COUNT_PRICE'], 0, ',', '.') }}</p>
                    </div>
                    <p>Please settle the payment by transferring it to the following bank account with your order number
                        as the reference. </p>
                    <p>BCA / 7310160371 / Davia Prayudi</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="line1"></div>
            </div>
        </div>
    </div>
</section>




@include('components.include.footer')

@endsection