@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', "")
@section('meta-author', '')
@section('meta-fb-title', "")
@section('meta-fb-type', "")
@section('meta-fb-desc', "")
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title', 'Checkout')
@section('content')
@include('components.include.header')


<!-- Breadcrumbs -->
<section class="section-timezone ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 align-self-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-4 text-center align-self-center">
                <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>

            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>

<!-- Checkout Item -->
<section class="section-checkout-item pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Order Summary</th>
                            <th scope="col" class="text-center">Price</th>
                            <th scope="col" class="text-center">Quantity</th>
                            <th scope="col" class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ((isset($_SESSION['CART']) && isset($_SESSION['CART']['DATA'])) ? $_SESSION['CART']['DATA'] : [] as $item)
                            <tr>
                                <td class="td-img">
                                    <div class="checkout-item">
                                        <img src="{{ $item['IMAGE'] }}" class="item-image" alt="">
                                        <div class="checkout-info">
                                            <p class="checkout-name">{{ $item['NAME'] }}</p>
                                            <p>
                                                @if ($item['SIZE'] == 1)
                                                    S
                                                @elseif ($item['SIZE'] == 2)
                                                    M
                                                @elseif ($item['SIZE'] == 3)
                                                    L
                                                @elseif ($item['SIZE'] == 4)
                                                    XL
                                                @endif
                                            </p>
                                            <p>{{ number_format($item['PRICE'], 0, ',', '.').' x '.$item['QTY'] }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">Rp. {{ number_format($item['PRICE'], 0, ',', '.') }}</td>
                                <td class="text-center">{{ $item['QTY'] }}</td>
                                <td class="text-center">Rp. {{ number_format($item['COUNT_PRICE'], 0, ',', '.') }}</td>
                            </tr>    
                        @empty
                            <tr>
                                <td class="text-center" colspan="4">Data Masih Kosong!</td>
                            </tr>
                        @endforelse
                        <tr>
                            <td colspan="2">
                            </td>
                            <td class="text-center">
                                TOTAL ITEM :
                            </td>
                            <td class="text-center" id="rp_count_price">
                                Rp. @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ number_format($_SESSION['CART']['COUNT_PRICE'], 0, ',', '.') }} @else 0 @endif
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<section class="section-profile py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('checkout-store') }}" method="POST" id="form_checkout">
                    {{ csrf_field() }}
                    <input type="hidden" name="shipping" id="shipping" value="0">
                    <input type="hidden" name="count_payment" id="hidden_count_payment" value="@if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ $_SESSION['CART']['COUNT_PRICE'] }} @else 0 @endif">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="mb-3">
                                <label class="form-label">Nama Lengkap</label>
                                <input type="text" name="name" class="form-control" placeholder="" autocomplete="off">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Provinsi</label>
                                <select name="province" id="province" class="form-select" aria-label="Default select example">
                                    <option selected>-- Pilih Provinsi --</option>
                                    @foreach ($province as $item)
                                        <option value="{{ $item['ID'] }}">{{ $item['PROV'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Kota/Kabupaten</label>
                                <select name="city" id="city" class="form-select" aria-label="Default select example">
                                    <option value="{{ null }}">-- Pilih Provinsi Terlebih Dahulu --</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Alamat Lengkap</label>
                                <input type="text" name="address" class="form-control" placeholder="" autocomplete="off">
                                <div class="form-text">(Nama Jalan, No rumah, RT/RW, patokan, Desa dan Kecamatan)</div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Kode pos</label>
                                <input type="text" name="postal_code" class="form-control" placeholder="" autocomplete="off">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Kurir</label>
                                <select name="courier" id="courier" class="form-select" aria-label="Default select example">
                                    <option selected>-- Pilih Provinsi & Kota/Kab terlebih dahulu -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-1">
                            <div class="mb-3">
                                <label class="form-label">No. Telepon</label>
                                <input type="number" name="phone" class="form-control" placeholder="" autocomplete="off">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="" autocomplete="off">
                            </div>
                            <div class="mb-3">
                                <label class="form-label ">Payment</label>
                                <div class="d-flex justify-content-between form-payment">
                                    <span>
                                        <div class="form-check">
                                            <input class="form-check-input" name="payment" required type="checkbox" value="BCA" id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                <img src="{{asset('assets')}}/img/icon/payment/bca.png" class="img-payment"
                                                    alt="bca">
                                            </label>
                                        </div>
                                    </span>
                                    <span>Transfer BCA</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="order-detail py-3">
                    <div class="mb-3">
                        <label class="form-label">Order Detail</label>
                        <div class="d-flex justify-content-between order-result">
                            <span>Item Total</span>

                            <span>
                                Rp. @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ number_format($_SESSION['CART']['COUNT_PRICE'], 0, ',', '.') }} @else 0 @endif    
                            </span>
                        </div>
                        <div class="d-flex justify-content-between order-result">
                            <span>Shipping</span>
                            <span id="rp_shipping">Rp. 0</span>
                        </div>
                        <div class="d-flex justify-content-between order-result">
                            <span>Total</span>
                            <span id="count_payment">
                                Rp. @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ number_format($_SESSION['CART']['COUNT_PRICE'], 0, ',', '.') }} @else 0 @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id risus felis nunc
                    velit.<br> Maecenas pharetra, non nam euismod vitae faucibus pharetra vestibulum.
                </div>
                <button class="btn btn-dark mt-2" id="btn_submit_checkout">Submit</button>
            </div>
        </div>
    </div>
</section>

@include('components.include.footer')
@endsection

@push('scripts')
    <script>
        // Onchange City
        $('#province').change(function() {
            let prov_id = $('#province').val();

            $.ajax({ 
                type: 'GET', 
                url: "{{ url('city') }}/"+prov_id,  
                dataType: 'json',
                success: function (data) { 
                    $('#city').html('');
                    $('#city').append(`<option value="">-- Pilih Kota/Kab --</option>`);

                    data.forEach(x => {
                        $('#city').append(`
                            <option value="`+ x.city_id +`">`+ x.type + ' ' + x.city_name +`</option>
                        `);
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.status);
                },
            });
        })

        // Onchange Courier
        $('#city').change(function() {
            let city_id = $('#city').val();

            $.ajax({ 
                type: 'GET', 
                url: "{{ url('cost') }}/"+city_id,  
                dataType: 'json',
                success: function (data) {
                    $('#courier').html('') 
                    if (data.rajaongkir.status.code == 200) {
                        $('#courier').append(`<option value="">-- Pilih Kurir --</option>`)

                        if (data.rajaongkir.results[0].costs.length > 0) {
                            data.rajaongkir.results[0].costs.forEach(x => {
                                $('#courier').append(`
                                    <option value="`+ data.rajaongkir.results[0].name + '-' + x.service +`">`+ data.rajaongkir.results[0].name + ' - ' + x.service + ' (' + x.cost[0].etd + ' Hari) Rp. ' + x.cost[0].value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +`</option>
                                `);
                            });   
                        } else {
                        $('#courier').html(`<option value="">-- Kurir tidak tersedia di wilayah anda --</option>`)
                        }
                    } else {
                        $('#courier').append(`<option value="">-- `+ data.rajaongkir.status.description +` --</option>`)
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.status);
                },
            });
        })

        $('#courier').change(function() {
            let count_price = $('#rp_count_price').text().replace(/\D/g,'');
            var value_shipping = ($('#courier option:selected').text().split(" Rp. ").pop()).replace(/\D/g,'');
            if (value_shipping == false) {
                value_shipping = "0";
            } 
            let rp_shipping = parseInt(count_price) + parseInt(value_shipping);
            $('#rp_shipping').html('Rp. '+value_shipping.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#count_payment').html('Rp. '+rp_shipping.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

            $('#shipping').val(value_shipping);
            $('#hidden_count_payment').val(rp_shipping);
        })

        $('#btn_submit_checkout').click(function() {
            $('#form_checkout').submit();
        })
    </script>
@endpush