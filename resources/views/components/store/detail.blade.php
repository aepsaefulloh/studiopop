@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', "")
@section('meta-author', '')
@section('meta-fb-title', "")
@section('meta-fb-type', "")
@section('meta-fb-desc', "")
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title')
    {{ $product['PRODUCT'] }}
@endsection

@section('content')
@include('components.include.header')

<!-- Breadcrumbs -->
<section class="section-timezone ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 align-self-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('/store')}}">Store</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Homebreaks Blue T-Shirt
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-4 text-center align-self-center">
                <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>

            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>


<section class="product-detail py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="feature-image">
                    <img id="img" src="{{ $product['IMAGE'][0]['NAME'] }}" class="img-fluid">
                </div>
                <div class="feature-select" id="thumb_img" class="cf">
                    @foreach ($product['IMAGE'] as $item)
                        <img @if ($item['KEY'] == 0) class="active" @endif src="{{ $item['NAME'] }}" onclick="changeimg('{{ $item['NAME'] }}',this);">
                    @endforeach
                </div>
            </div>
            <div class="col-md-5 offset-md-2">
                <div class="product-name">
                    <h4>{{ $product['PRODUCT'] }}</h4>
                    <hr>
                </div>
                <form method="#">
                    <div class="store-heading">
                        <p>SIZE :</p>
                        <div class="product-size mb-4">
                            @forelse ($product['SIZE'] as $item)
                                <input type='radio' name='size' value='{{ $item['SIZE'] }}' required>&nbsp; @if ($item['SIZE'] == 1) S @elseif ($item['SIZE'] == 2) M @elseif($item['SIZE'] == 3) L @elseif($item['SIZE'] == 4) XL @endif
                            @empty
                                <h6 class="text-center">Size Kosong!</h6>
                            @endforelse
                        </div>
                    </div>

                    <div class="store-heading mb-4">
                        <p>QTY :</p>
                        <input type="number" name="qty" class="form-control w-25" value="1" id="qty" autocomplete="off" min="1" max="100">
                    </div>

                    <div class="store-heading">
                        <p>PRICE :</p>
                        <h5 class="mb-3">Rp. {{ number_format($product['PRICE'], 0, ',', '.') }}</h5>
                    </div>

                    <button type="button" class="btn btn-dark w-100 my-3 disabled" id="btn_add_cart"><?php echo ($product['AVAIL'] == 1) ? 'ADD TO CART' : 'SOLD OUT'; ?></button>

                </form>
                <div class="info-product">
                    <h6 class="text-muted mt-3"><strong>PRODUCT INFO :</strong></h6>
                    <div class="product-info">
                        <p class="text-muted">{!! $product['SPECS'] !!}</p>
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th scope="col">SIZE</th>
                                    <th scope="col">WIDTH</th>
                                    <th scope="col">LENGTH</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($product['SIZE'] as $item)
                                    <tr>
                                        <td>
                                            @if ($item['SIZE'] == 1)
                                                S
                                            @elseif ($item['SIZE'] == 2)
                                                M
                                            @elseif ($item['SIZE'] == 3)
                                                L
                                            @elseif ($item['SIZE'] == 4)
                                                XL
                                            @endif
                                        </td>
                                        <td>{{ $item['WIDTH'] }}</td>
                                        <td>{{ $item['LENGTH'] }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">Size Masih Kosong!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



@include('components.include.footer')
@endsection

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
         $("input[name=size]:radio").click(function () {
            if ($('input[name=size]:checked').val() && "<?php echo $product['AVAIL']; ?>" == 1) {
                $('#btn_add_cart').removeClass('disabled');
            } else if ($('input[name=size]:checked').val() && "<?php echo $product['AVAIL']; ?>" == 0) {
                $('#btn_add_cart').addClass('disabled').html('SOLD OUT');
            } else {
                $('#btn_add_cart').addClass('disabled');
            }
        });

        // Onchange QTY
        $("#qty").on('mouseup change click', function() {
          var min = parseInt($(this).attr('min'));
          
          if ($(this).val() < min) {
            $(this).val(min);
            alert('QTY Minimal '+min)
          }       
        }); 

        // Button Add Cart
        $('#btn_add_cart').on('click', function() {
            // Result Data Form
            let img_product = "{{ $product['IMAGE'][0]['NAME'] }}";
            let code_product = "{{ $product['CODE'] }}";
            let name_product = "{{ $product['PRODUCT'] }}";
            let size_product = $('input[name=size]:checked').val();
            let qty_product = $('#qty').val();
            let price_product = "{{ $product['PRICE'] }}";

            $.ajax({
                url:"{{ route('add_cart') }}",
                type:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    "image" : img_product,
                    "code" : code_product,
                    "name" : name_product,
                    "size" : size_product,
                    "qty" : qty_product,
                    "price" : price_product
                },
                success:function(response) {
                    // Push Count Cart to badge
                    $('#count_cart').html(response);


                    // Sweetalert js
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Product success add to cart",
                        showConfirmButton: false,
                        timer: 1500
                    })

                }
            })
        })
    </script>
@endpush