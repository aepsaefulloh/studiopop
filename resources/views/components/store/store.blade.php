@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title', 'Store')
@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')


<section class="section-store my-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <p>VIEW THE COLLECTION</p>
                    <h1>Store</h1>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($product as $item)
                <div class="col-md-3 col-6">
                    <figure class="figure">
                        <a href="{{ url('/store-detail'.'/'.$item['ID'].'/'.Helper::url_slug($item['PRODUCT'])) }}">
                        <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid" alt="{{ $item['PRODUCT'] }}">
                        <figcaption class="figure-caption ps-3">
                            @if ($item['AVAIL'] == 1)
                                <span class="text-primary"></span>
                            @else
                                <span class="text-danger">SOLD OUT</span>
                            @endif
                            <h3>{{ $item['PRODUCT'] }}</h3>
                            <p>Rp. {{ number_format($item['PRICE'], 0, ',', '.') }}</p>
                        </figcaption>
                    </a>
                    </figure>
                </div>
            @empty
                <div class="col-12">
                    <h5 class="text-center">Product Masih Kosong!</h5>    
                </div>                
            @endforelse
    </div>
    <div class="mt-5 col-12">
        <div class="d-flex justify-content-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                  <li class="page-item @if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) disabled @endif">
                    <a class="page-link" href="{{ (isset($_GET['page'])) ? url('store?page='.($_GET['page']-1)) : url('store?page=1') }}" tabindex="-1" aria-disabled="true">Previous</a>
                  </li>
                  @for ($i = 1; $i <= $paginate_all; $i++)
                    <li class="page-item @if ((!isset($_GET['page']) && 1 == $i) ||(isset($_GET['page']) && $_GET['page'] == $i)) disabled active @endif" aria-current="page">
                        <a class="page-link" href="{{ url('store?page=').$i }}">{{ $i }}</a>
                    </li>
                  @endfor
                  <li class="page-item @if ((!isset($_GET['page']) && 1 == $paginate_all) || (isset($_GET['page']) && $_GET['page'] == $paginate_all)) disabled @endif">
                    <a class="page-link" href="{{ (isset($_GET['page'])) ? url('store?page='.($_GET['page']+1)) : url('store?page=2') }}">Next</a>
                  </li>
                </ul>
            </nav>
        </div>
    </div>
    </div>
</section>



@include('components.include.footer')

@endsection