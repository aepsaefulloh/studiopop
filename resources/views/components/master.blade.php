<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="canonical" href="studiopop.id" />
    <meta name="description"
        content="Studio Pop adalah proyek eksperimen dalam membuat sebuah ekosistem bagi para produser, musisi, seniman dan juga pelaku industri untuk berkolaborasi dalam sebuah karya seni.">
    <meta property="og:price:currency" content="IDR">
    <meta name="author" content="sketsahouse.com">
    <!-- Facebook & Twitter Meta -->
    <meta property="og:title" content="Studiopop">
    <meta property="og:type" content="website" />
    <meta property="og:description"
        content="Studio Pop adalah proyek eksperimen dalam membuat sebuah ekosistem bagi para produser, musisi, seniman dan juga pelaku industri untuk berkolaborasi dalam sebuah karya seni.">
    <meta property="og:image" content="{{asset('assets')}}/img/logo/main-logo.png">
    <meta property="og:url" content="https://www.studiopop.id">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@studiopop" />
    <meta name="twitter:title" content="Studiopop" />
    <meta name="twitter:description"
        content="Studio Pop adalah proyek eksperimen dalam membuat sebuah ekosistem bagi para produser, musisi, seniman dan juga pelaku industri untuk berkolaborasi dalam sebuah karya seni." />
    <meta name="twitter:image" content="{{asset('assets')}}/img/logo/main-logo.png" />
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets')}}/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets')}}/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets')}}/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets')}}/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets')}}/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets')}}/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets')}}/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets')}}/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets')}}/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assets')}}/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets')}}/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets')}}/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets')}}/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{asset('assets')}}/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('assets')}}/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>@yield('title') | Studio Pop</title>
    @include('components.include.scriptHeader')
</head>

<body>
    @yield('content')
    @include('components.include.scriptFooter')
</body>

</html>