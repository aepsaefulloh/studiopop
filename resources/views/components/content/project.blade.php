@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title', 'Project')
@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')

<section class="py-5 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <div class="main-heading">
                <h1>Project</h1>
                {{-- <p>Studio Pop Season 2 </p> --}}
            </div>
        </div>
    </div>
</section>
<section class="section-article">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="grid-products">
                    @forelse ($project as $item)
                        <figure class="figure">
                            <a href="{{ url('/project-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                            <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid " alt="{{ $item['IMAGE'] }}">
                            <figcaption class="figure-caption">{{ $item['TITLE'] }}</figcaption>
                            </a>
                        </figure>
                    @empty
                        <h5 class="text-center">Data Masih Kosong!</h5>
                    @endforelse 
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <nav aria-label="...">
                        <ul class="pagination">
                          <li class="page-item @if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) disabled @endif">
                            <a class="page-link" href="{{ (isset($_GET['page'])) ? url('project?page='.($_GET['page']-1)) : url('project?page=1') }}" tabindex="-1" aria-disabled="true">Previous</a>
                          </li>
                          @for ($i = 1; $i <= $paginate_all; $i++)
                            <li class="page-item @if ((!isset($_GET['page']) && 1 == $i) ||(isset($_GET['page']) && $_GET['page'] == $i)) disabled active @endif" aria-current="page">
                                <a class="page-link" href="{{ url('project?page=').$i }}">{{ $i }}</a>
                            </li>
                          @endfor
                          <li class="page-item @if ((!isset($_GET['page']) && 1 == $paginate_all) || (isset($_GET['page']) && $_GET['page'] == $paginate_all)) disabled @endif">
                            <a class="page-link" href="{{ (isset($_GET['page'])) ? url('project?page='.($_GET['page']+1)) : url('project?page=2') }}">Next</a>
                          </li>
                        </ul>
                      </nav>
                </div>
                <div class="line1 my-3"></div>
            </div>
        </div>
    </div>
</section>
<section class="section-article my-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <p>VIEW THE COLLECTION</p>
                    <h1>Pop ‘n Roll</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="grid-products">
                    @forelse ($pop_roll_current as $item)
                        <figure class="figure">
                            <a href="{{ url('/pop-n-roll-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                            <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid " alt="{{ $item['IMAGE'] }}">
                        </a>
                        </figure>
                    @empty
                        <h5 class="text-center">Data Masih Kosong!</h5>
                    @endforelse 
                </div>
            </div>
        </div>
    </div>
</section>


@include('components.include.footer')

@endsection