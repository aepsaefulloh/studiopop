@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title')
    {{ Helper::url_slug($journal['TITLE']) }}
@endsection

@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')


<section class="py-4 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <div class="main-heading">
                <h1>{{ $journal['TITLE'] }}</h1>
            </div>
        </div>
    </div>
    <div class="line1 mt-0"></div>
</section>
<section class="section-content-detail mt-md-5">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <div class="content-detail top">
                    <img src="{{ $journal['IMAGE'] }}" class="img-fluid" alt="{{ $journal['TITLE'] }} - {{ Date('Y-m-d', strtotime($journal['CREATE_TIMESTAMP'])) }}">
                    <div>{!! $journal['CONTENT'] !!}</div>
                </div>
                <div class="content-detail bottom">
                    <div class="content"></div>
                </div>
            </div>
        </div>
    </div>
</section>


@include('components.include.footer')

@endsection