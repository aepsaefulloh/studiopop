@extends('components.master')
@section('title', 'Journal')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')

<!-- section Article -->
<section class="section-article my-3 my-md-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <h1>Journal</h1>
                    <div class="line1"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="grid-products"> 
                    @forelse ($journal as $item)
                        <figure class="figure">
                            <a href="{{ url('/journal-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                            <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid " alt="{{ $item['IMAGE'] }}">
                            <figcaption class="figure-caption">{{ $item['TITLE'] }}</figcaption>
                            </a>
                        </figure>
                    @empty
                        <h5 class="text-center">Data Masih Kosong!</h5>  
                    @endforelse
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <nav aria-label="...">
                        <ul class="pagination">
                          <li class="page-item @if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) disabled @endif">
                            <a class="page-link" href="{{ (isset($_GET['page'])) ? url('journal?page='.($_GET['page']-1)) : url('journal?page=1') }}" tabindex="-1" aria-disabled="true">Previous</a>
                          </li>
                          @for ($i = 1; $i <= $paginate_all; $i++)
                            <li class="page-item @if ((!isset($_GET['page']) && 1 == $i) || (isset($_GET['page']) && $_GET['page'] == $i)) disabled active @endif" aria-current="page">
                                <a class="page-link" href="{{ url('journal?page=').$i }}">{{ $i }}</a>
                            </li>
                          @endfor
                          <li class="page-item @if ((!isset($_GET['page']) && 1 == $paginate_all) || (isset($_GET['page']) && $_GET['page'] == $paginate_all)) disabled @endif">
                            <a class="page-link" href="{{ (isset($_GET['page'])) ? url('journal?page='.($_GET['page']+1)) : url('journal?page=2') }}">Next</a>
                          </li>
                        </ul>
                      </nav>
                </div>
                <div class="line1"></div>
            </div>
        </div>
    </div>
</section>
<section class="section-article my-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <p>VIEW THE COLLECTION</p>
                    <h1>Pop ‘n Roll</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="grid-products">
                    @forelse ($pop_roll_current as $item)
                        <figure class="figure">
                            <a href="{{ url('/pop-n-roll-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                            <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid " alt="{{ $item['IMAGE'] }}">
                        </a>
                        </figure>
                    @empty
                        <h5 class="text-center">Data Masih Kosong!</h5>
                    @endforelse 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section Article End -->



@include('components.include.footer')

@endsection