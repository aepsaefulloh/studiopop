@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title')
    {{ $pop_roll['TITLE'] }}
@endsection
@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')


<section class="py-4 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <div class="main-heading">
                <h1>{{ $pop_roll['TITLE'] }}</h1>
                <p>{{ $pop_roll['SUMMARY'] }} | {{ Date('Y-m-d', strtotime($pop_roll['CREATE_TIMESTAMP'])) }}</p>
            </div>
        </div>
    </div>
    <div class="line1 mt-0"></div>
</section>
<section class="section-content-detail section-popnroll mt-md-5">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-9">
                <div class="content-detail top">
                    <img src="{{ $pop_roll['IMAGE'] }}"
                        class="img-fluid" alt="{{ $pop_roll['TITLE'] }}">

                </div>
                <div class="content-detail bottom">
                    <div class="content">
                        <div>{!! $pop_roll['CONTENT'] !!}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sportify-embed">
                    <iframe src="{{ $pop_roll['SPORTIFY'] }}"
                        style="border: 0; width: 100%; height: 500px;" allowfullscreen allow="encrypted-media"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>


@include('components.include.footer')
@endsection