@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-author', '')
@section('meta-fb-title', '')
@section('meta-fb-type', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title')
    {{ Helper::url_slug($project['TITLE']) }}
@endsection

@section('content')
    @include('components.include.header')
    @include('components.include.breadcrumb')


    <!-- Hero -->
    <section class="section-hero section-journal">
        <div class="container-fluid pt-3 pt-md-4">
            {{-- <div class="hero image-as-background" style="background-image: url('{{ $project['IMAGE'] }}');">
                
            </div> --}}
            <div class="vids">
                <div class="owl-carousel owl-theme"> 
        
                    <div class="item-video" data-merge="{{$project['ID']}}"><a class="owl-video" href="{{ $project['YOUTUBE'] }}"></a></div>
    
                </div> 
            </div>
         
            <header class="hero-container">
                <p>{{ $project['CREATE_BY'] }}</p>
                <h1 class="hero-title">{{ $project['TITLE'] }}</h1>
            </header>
            <div class="line1"></div>
        </div>
    </section>
    <!-- Hero-End -->

    <!-- section Article -->
    <section class="section-article my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="main-heading">
                        <p>VIEW THE COLLECTION</p>
                        <h1>Journal</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="grid-products">
                        @forelse ($journal_current as $item)
                            <figure class="figure">
                                <a
                                    href="{{ url('/pop-n-roll-detail') . '/' . $item['ID'] . '/' . Helper::url_slug($item['TITLE']) }}">
                                    <img src="{{ $item['IMAGE'] }}" class="figure-img img-fluid "
                                        alt="{{ $item['IMAGE'] }}">
                                </a>
                            </figure>
                        @empty
                            <h5 class="text-center">Data Masih Kosong!</h5>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Article End -->



    @include('components.include.footer')

@endsection


@push('scripts')
<script>
    $('.owl-carousel').owlCarousel({
    items: 1,
    loop: true,
    video: true,
    lazyLoad: false,
    mouseDrag: false,
}); 
</script>
    
@endpush



