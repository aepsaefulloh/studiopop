@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', "")
@section('meta-author', '')
@section('meta-fb-title', "")
@section('meta-fb-type', "")
@section('meta-fb-desc', "")
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
@section('meta-twitter-card', 'summary_large_image')

@section('title', 'Home')
@section('content')
@include('components.include.header')
@include('components.include.breadcrumb')
<div class="container">
    <img src="{{ $about['FILENAME'] }}" class="w-100 mt-5 mb-5" alt="{{ $about['FILENAME'] }}">
</div>  
@include('components.include.footer')
@endsection

  
  
  
  
  

