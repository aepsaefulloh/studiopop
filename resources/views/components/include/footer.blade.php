<footer>
    <div class="container-fluid">
        <div class="footer-right">
            <a href="{{ (isset(Helper::config_instagram()['CVALUE'])) ? Helper::config_instagram()['CVALUE'] : null }}" target="_blank"><i class="bi bi-instagram"></i></a>
            <a href="{{ (isset(Helper::config_youtube()['CVALUE'])) ? Helper::config_youtube()['CVALUE'] : null }}" target="_blank"><i class="bi bi-youtube"></i></a>
        </div>
        <div class="footer-left">
            <div class="footer-links">
                Copyrights © 2021 All Rights Reserved by {{ (isset(Helper::config_name_apps()['CVALUE'])) ? Helper::config_name_apps()['CVALUE'] : null }}.
            </div>
            <p>{{ (isset(Helper::config_desc()['CVALUE'])) ? Helper::config_desc()['CVALUE'] : null }}</p>
        </div>
    </div>
</footer>