<section class="section-timezone ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 align-self-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center justify-content-md-start mb-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page" style="text-transform: capitalize;">{{ Request::segment(1) }}
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-4 text-center align-self-center">
                <span>Jakarta | <span id="date"></span> | <span id="time"></span></span>

            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>