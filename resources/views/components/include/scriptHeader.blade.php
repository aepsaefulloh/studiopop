<link rel="stylesheet" href="{{ asset('assets') }}/plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">
<link rel="stylesheet" href="{{ asset('assets') }}/css/responsive.css">
<link rel="stylesheet" href="{{ asset('assets') }}/plugins/swiperjs/swiper-bundle.min.css">
<link rel="stylesheet" href="{{ asset('assets') }}/plugins/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="{{ asset('assets') }}/plugins/owl-carousel/owl.theme.default.min.css">


