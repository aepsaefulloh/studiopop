<script src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('assets') }}/plugins/bootstrap/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/plugins/swiperjs/swiper-bundle.min.js"></script>
<script src="{{ asset('assets') }}/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="{{ asset('assets') }}/js/app.js"></script>


@stack('scripts')
