<!-- Navbar -->
<nav class="navbar navbar-expand-sm  sticky-top navbar-light bg-light main-nav">
    <div class="container-fluid justify-content-center d-none d-md-flex">
        <ul class="nav navbar-nav w-100 flex-nowrap">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('/about') }}">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/project') }}">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/journal') }}">Journal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/pop-n-roll') }}">Pop 'n Roll</a>
            </li>
        </ul>
        <ul class="nav navbar-nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">
                    <img src="{{ isset(Helper::config_logo()['CVALUE']) ? Helper::config_logo()['CVALUE'] : null }}"
                        class="main-logo"
                        alt="{{ isset(Helper::config_logo()['CVALUE']) ? Helper::config_logo()['CVALUE'] : null }}">
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav w-100 justify-content-end">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/store') }}">Store</a>
            </li>
            <li class="nav-item">
            <li class="nav-link" onclick="window.location.href='{{ url('/cart') }}';">
                <div class="add-cart">
                    <img src="{{ asset('assets') }}/img/icon/cart.png" class="icon-cart" alt="cart">
                    <span class="badge bg-danger" id="count_cart">@if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_QTY']) && $_SESSION['CART']['COUNT_QTY'] != null) {{ $_SESSION['CART']['COUNT_QTY'] }} @else 0 @endif</span>
                </div>
            </li>
            </li>
        </ul>
    </div>
    <div class="container-fluid justify-content-between d-flex d-md-none">
        <ul class="nav navbar-nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">
                    <img src="{{ isset(Helper::config_logo()['CVALUE']) ? Helper::config_logo()['CVALUE'] : null }}"
                        class="main-logo"
                        alt="{{ isset(Helper::config_logo()['CVALUE']) ? Helper::config_logo()['CVALUE'] : null }}">
                </a>
            </li>
        </ul>
        <div class="add-cart" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight"
            aria-controls="offcanvasRight">
            <svg viewBox="0 0 150 50" width="40" height="40" fill="#000" fill-opacity=".6">
                <rect width="50" height="10"></rect>
                <rect y="25" width="100" height="10"></rect>
                <rect y="50" width="70" height="10"></rect>
            </svg>
        </div>
    </div>


</nav>
<!-- Navbar-End -->

{{-- Canvas Cart --}}
<div class="offcanvas offcanvas-end offcanvas-cart" tabindex="-1" id="offcanvasRight"
    aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <a class="nav-link nav-offcanvas" href="{{ url('/') }}">
            <img src="{{ asset('assets') }}/img/logo/main-logo.png" class="offcanvas-logo" alt="offcanvas-logo">
        </a>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <ul class="nav navbar-nav w-100 flex-nowrap">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('/about') }}">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/project') }}">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/journal') }}">Journal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/pop-n-roll') }}">Pop 'n Roll</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/store') }}">Store</a>
            </li>
        </ul>
    </div>
</div>


<div class="floating-icon" id="shopIconMobile">
    <button type="button"  class="btn btn-light shadow position-relative" onclick="window.location.href='{{ url('/cart') }}';">
        <img src="{{ asset('assets') }}/img/icon/cart.png" class="icon-cart" alt="cart">
        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_QTY']) && $_SESSION['CART']['COUNT_QTY'] != null) {{ $_SESSION['CART']['COUNT_QTY'] }} @else 0 @endif
        </span>
    </button>
</div>


@push('scripts')
    <script>
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 180) {
                $('#shopIconMobile').fadeIn();
            } else {
                $('#shopIconMobile').fadeOut();
            }
        });
    </script>
@endpush
