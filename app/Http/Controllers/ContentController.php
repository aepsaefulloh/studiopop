<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;

class ContentController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function about(){
        // Data About Banner
        $about = $this->http_get($this->url_api().'content-banner/about?limit=1');
        
        if ($about && $about['status'] == 200 && count($about['data']) > 0) {
            $this->data['about'] = $about['data'][0];
        } else {
            $this->data['about'] = ["FILENAME" => null];
        }
        
        return view('components.content.about', $this->data);
    }

    public function project(Request $request){
        $perpage = 10;
        $page = ($request->page != '') ? $request->page : 1;

        // Data Project
        $project = $this->http_get($this->url_api().'content?category=4&page='.$page.'&perpage='.$perpage);
        
        if ($project && $project['status'] == 200 && count($project['data']) > 0) {
            $this->data['project'] = $project['data'];
            
            $this->data['paginate_all'] = ceil($project['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['project'] = [];
            $this->data['paginate_all'] = 0;
        }
        $this->data['pop_roll_current'] = $this->get_content(2, 1, 5);
        
        return view('components.content.project', $this->data);
        
    }

    public function ProjectDetail($id, $title){
        
        // Data Project Find
        $project = $this->http_get($this->url_api().'content/'.$id);
        $this->data['journal_current'] = $this->get_content(1, 1, 5);
        
        if ($project && $project['status'] == 200 && isset($project['data']['IMAGE'])) {
            $this->data['project'] = $project['data'];
            // dd($this->data['project']);

            return view('components.content.project-detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }

    }

    public function Journal(Request $request){
        $perpage = 10;
        $page = ($request->page != '') ? $request->page : 1;

        // Data Journal
        $journal = $this->http_get($this->url_api().'content?category=1&page='.$page.'&perpage='.$perpage);
        
        if ($journal && $journal['status'] == 200 && count($journal['data']) > 0) {
            $this->data['journal'] = $journal['data'];
            
            $this->data['paginate_all'] = ceil($journal['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['journal'] = [];

            $this->data['paginate_all'] = 0;
        }
        $this->data['pop_roll_current'] = $this->get_content(2, 1, 5);

        return view('components.content.journal', $this->data);
    }

    public function JournalDetail($id, $title){
        // Data Journal Find
        $journal = $this->http_get($this->url_api().'content/'.$id);
        
        if ($journal && $journal['status'] == 200 && isset($journal['data']['IMAGE'])) {
            $this->data['journal'] = $journal['data'];
            
            return view('components.content.journal-detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }
        
    }
    

    public function PopnRoll(Request $request){
        $perpage = 10;
        $page = ($request->page != '') ? $request->page : 1;

        // Data Pop and Roll
        $pop_roll = $this->http_get($this->url_api().'content?category=2&page='.$page.'&perpage='.$perpage);
        
        if ($pop_roll && $pop_roll['status'] == 200 && count($pop_roll['data']) > 0) {
            $this->data['pop_roll'] = $pop_roll['data'];
            
            $this->data['paginate_all'] = ceil($pop_roll['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['pop_roll'] = [];

            $this->data['paginate_all'] = 0;
        }
        return view('components.content.popnroll', $this->data);
    }

    public function PopnRollDetail($id, $title){
        // Data Pop And Roll Find
        $pop_roll = $this->http_get($this->url_api().'content/'.$id);
        
        if ($pop_roll && $pop_roll['status'] == 200 && isset($pop_roll['data']['IMAGE'])) {
            $this->data['pop_roll'] = $pop_roll['data'];
            
            return view('components.content.popnroll-detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }
    }
}