<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;  
    // -------------------------------------------------- URL API DEFAULT ----------------------------------------- //
    public function url_api() {
        // return 'http://192.168.1.218:8001/api/';
        return 'http://demoapi.studiopop.id/api/';
    }

    public function KEY_RAJAONGKIR() {
        return '920ccbff1349d25bdd4fed0422abc2e9';
    }

    public function API_KEY() {
        // return '$2a$10$vm9zPtIala1j4GjBTuSkDOdrCQ9/tlFruMQQ7iwsxVCZryU1Dv7fW';
        return env('TOKEN_APP');
    }

    // ------------------------------------------------- HTTP CLIENT ---------------------------------------------- //
    public function http_get($url) {
        $http_get = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => $this->API_KEY()
        ])->get($url);
        // dd($http_get->body());
        if ($http_get->ok()) {
            return $http_get;
        } elseif ($http_get['status'] == 401) {
            return abort(401, 'Token Apps Failed!');
        } else {
            return response()->json([
                "status"    => 500,
                "message"   => "API Server Error!"
            ]);
        }   
    }

    public function http_post($url, $data) {
        $http_post = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => $this->API_KEY()
        ])->post($url, $data);
        // dd($http_post->body());
        if ($http_post->ok()) {
            return $http_post;
        } elseif ($http_post['status'] == 401) {
            return abort(401, 'Token Apps Failed!');
        } else {
            return response()->json([
                "status"    => 500,
                "message"   => "API Server Error!"
            ]);
        }   
    }

    // ------------------------------------ Get API data content universal ------------------------------------------ //
    public function get_content($category, $page=1, $perpage=10) {
        // Data Content
        $pop_roll = $this->http_get($this->url_api().'content?category='.$category.'&page='.$page.'&perpage='.$perpage);
        
        if ($pop_roll && $pop_roll['status'] == 200 && count($pop_roll['data']) > 0) {
            $this->data['pop_roll'] = $pop_roll['data'];
        } else {
            $this->data['pop_roll'] = [];
        }

        return $this->data['pop_roll'];
    }

    // ----------------------------------- Rajaongkir API --------------------------------------------------------------- //
    function getCity($prov_id){
        //Get Data Kabupaten 
        $curl = curl_init();  
        curl_setopt_array($curl, array( 
          CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=".$prov_id, 
          CURLOPT_RETURNTRANSFER => true, 
          CURLOPT_ENCODING => "", 
          CURLOPT_MAXREDIRS => 1000, 
          CURLOPT_TIMEOUT => 30, 
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
          CURLOPT_CUSTOMREQUEST => "GET", 
          CURLOPT_HTTPHEADER => array( 
            "Key: ".$this->KEY_RAJAONGKIR()
          ), 
        )); 
      
        $response = curl_exec($curl); 
        $err = curl_error($curl); 
        $data=json_decode($response, true); 
        curl_close($curl); 
        
        if ($data['rajaongkir']['status']['code'] ==200) {
            return $data['rajaongkir']['results'];
        } else {
            return $data;
        }
    }

    function getCost($destination){
        $origin=151;
        $curl = curl_init();
      
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=".$origin."&destination=".$destination."&weight=500&courier=jne",
        CURLOPT_HTTPHEADER => array(
          "content-type: application/x-www-form-urlencoded",
          "key: ".$this->KEY_RAJAONGKIR()
            ),
          ));
          
      
        $response = curl_exec($curl); 
        $err = curl_error($curl); 
        $data=json_decode($response, true); 
        curl_close($curl); 
        
        return $data;
      }
}
