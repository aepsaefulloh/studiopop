<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(){
        // Data Hero Banner
        $hero_banner = $this->http_get($this->url_api().'content/journal-playlist?limit=3');
        
        if ($hero_banner && $hero_banner['status'] == 200) {
            $this->data['hero_banner'] = $hero_banner['data'];
        } else {
            $this->data['hero_banner'] = [];
        }
        // dd($this->data['hero_banner']);

        // Data Journal
        $project = $this->http_get($this->url_api().'content?category=4&perpage=5');
        
        if ($project && $project['status'] == 200) {
            $this->data['project'] = $project['data'];
        } else {
            $this->data['project'] = [];
        }

        // Data Product
        $product = $this->http_get($this->url_api().'product?page=1&perpage=4');
        
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
        } else {
            $this->data['product'] = [];
        }
        
        return view('home', $this->data);
    }
}