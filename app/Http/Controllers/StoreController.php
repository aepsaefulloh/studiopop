<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class storeController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(Request $request){
        $perpage = 12;
        $page = ($request->page != '') ? $request->page : 1;

        // Data Product
        $product = $this->http_get($this->url_api().'product?page='.$page.'&perpage='.$perpage);
        
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
            $this->data['paginate_all'] = ceil($product['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['product'] = [];
            $this->data['paginate_all'] = 0;
        }

        return view('components.store.store', $this->data);

    }
    public function detail($id, $title){
        // Data Product Find
        $product = $this->http_get($this->url_api().'product/'.$id);
        if ($product && $product['status'] == 200 && isset($product['data']['IMAGE'])) {
            $this->data['product'] = $product['data'];
            // dd($this->data['product']);
            return view('components.store.detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }
    }
}
