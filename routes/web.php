<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/city/{prov_id}', [Controller::class, 'getCity']);
Route::get('/cost/{destination}', [Controller::class, 'getCost']);

Route::get('/about', [ContentController::class, 'about'])->name('about');
Route::get('/project', [ContentController::class, 'project'])->name('project');
Route::get('/project-detail/{id}/{title}', [ContentController::class, 'ProjectDetail'])->name('project-detail');

Route::get('/journal', [ContentController::class, 'journal'])->name('journal');
Route::get('/journal-detail/{id}/{title}', [ContentController::class, 'JournalDetail'])->name('journal-detail');

Route::get('/pop-n-roll', [ContentController::class, 'popnroll'])->name('popnroll');
Route::get('/pop-n-roll-detail/{id}/{title}', [ContentController::class, 'PopnRollDetail'])->name('popnroll-detail');

Route::get('/store', [StoreController::class, 'index'])->name('store');
Route::get('/store-detail/{id}/{title}', [StoreController::class, 'detail'])->name('store-detail');

Route::get('/profile', [PaymentController::class, 'index'])->name('profile');
Route::post('/checkout-store', [PaymentController::class, 'checkout_store'])->name('checkout-store');

Route::post('/add-cart', [PaymentController::class, 'add_cart'])->name('add_cart');
Route::get('/remove-cart', [PaymentController::class, 'remove_cart'])->name('remove_cart');
Route::get('/cart', [PaymentController::class, 'cart'])->name('cart');

Route::get('/order-success', [PaymentController::class, 'orderSuccess'])->name('order-success');