window.setTimeout("waktu()", 1000);

function waktu() {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes(0) + ":" + today.getSeconds();
    var date = today.toDateString();
    setTimeout("waktu()", 1000);
    document.getElementById('time').innerHTML = time;
    document.getElementById('date').innerHTML = date;
}


function changeimg(url, e) {
    document.getElementById("img").src = url;
    let nodes = document.getElementById("thumb_img");
    let img_child = nodes.children;
    for (i = 0; i < img_child.length; i++) {
        img_child[i].classList.remove('active')
    }
    e.classList.add('active');
}

function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}
